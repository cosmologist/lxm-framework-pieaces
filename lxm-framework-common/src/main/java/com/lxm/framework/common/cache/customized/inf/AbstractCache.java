package com.lxm.framework.common.cache.customized.inf;


import com.lxm.framework.common.cache.customized.impl.ImmutableKey;
import com.lxm.framework.common.cache.customized.parts.CacheTarget;
import com.lxm.framework.common.utils.DateTimeUtils;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;

/**
 * @Author: Lys
 * @Date 2023/1/13
 * @Describe
 **/
public abstract class AbstractCache<K, V> implements Cache<K, V> {

    private static final long serialVersionUID = -7656172679665345803L;

    protected CacheListener<K, V> listener;

    protected Map<Immutable<K>, CacheTarget<K, V>> cacheMap;
    /**
     * 访问次数，get，set方法每调用一次，+1
     */
    protected LongAdder visitCount = new LongAdder();

    @Override
    public boolean exist(K key) {
        Immutable<K> mutableKey = ImmutableKey.of(key);
        return exist(mutableKey);
    }

    protected boolean exist(Immutable<K> key) {
        CacheTarget<K, V> v = cacheMap.get(key);
        return safe(v);
    }

    /**
     * 存储对象是否安全。不安全的定义：不存在，或过期
     * 如果过期，则删除
     *
     * @param target
     * @return
     */
    protected boolean safe(CacheTarget<K, V> target) {
        if (Objects.nonNull(target)) {
            if (target.isExpired()) {
                onRemove(target.getKey(), target.getValue());
            }
            return true;
        }
        return false;
    }

    @Override
    public void put(K key, V value, long timeout, TimeUnit timeUnit) {
        visitCount.increment();
        if (Objects.isNull(timeUnit)) {
            timeUnit = TimeUnit.MINUTES;
        }
        Immutable<K> immutableKey = ImmutableKey.of(key);
        synchronized (immutableKey.get()) {
            CacheTarget<K, V> target = new CacheTarget<>(key, value, timeout, timeUnit);
            cacheMap.put(immutableKey, target);
        }
    }

    @Override
    public V get(K key, boolean refresh) {
        visitCount.increment();
        Immutable<K> immutableKey = ImmutableKey.of(key);
        synchronized (immutableKey.get()) {
            CacheTarget<K, V> target = cacheMap.get(immutableKey);
            boolean safe = safe(target);
            if (safe) {
                if (refresh) {
                    target.refresh();
                }
                return target.getValue();
            }
            return null;
        }
    }

    @Override
    public V getThenRemove(K key) {
        V value = get(key, false);
        if (Objects.nonNull(value)) {
            onRemove(key, value);
            remove(key);
        }
        return value;
    }

    @Override
    public long getTimeout(K key) {
        Immutable<K> immutableKey = ImmutableKey.of(key);
        synchronized (immutableKey.get()) {
            CacheTarget<K, V> target = cacheMap.get(immutableKey);
            boolean safe = safe(target);
            if (safe) {
                final LocalDateTime expiredTime = target.getExpiredTime();
                return DateTimeUtils.toMillis(expiredTime) - System.currentTimeMillis();
            }
            return -1L;
        }
    }

    /**
     * 删除并且通知监听
     *
     * @param key
     * @param value
     */
    public void onRemove(K key, V value) {
        if (Objects.nonNull(this.listener)) {
            this.listener.onRemove(key, value);
        }
    }

    /**
     * 删除，同时触发监听
     *
     * @param key
     */
    @Override
    public void remove(K key) {
        this.cacheMap.remove(ImmutableKey.of(key));
    }

    public long visitCount() {
        return visitCount.sum();
    }

    @Override
    public void clear() {
        this.cacheMap.clear();
    }

    @Override
    public Iterator<CacheTarget<K, V>> iterator() {
        return this.cacheMap.values().iterator();
    }

    public void expireListener(CacheListener<K, V> listener) {
        this.listener = listener;
    }
}
