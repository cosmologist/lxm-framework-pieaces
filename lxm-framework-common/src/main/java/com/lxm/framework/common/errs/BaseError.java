package com.lxm.framework.common.errs;

/**
 * @Author: Lys
 * @Date 2023/1/9
 * @Describe
 **/
public interface BaseError {

    int getCode();

    String getMessage();
}
