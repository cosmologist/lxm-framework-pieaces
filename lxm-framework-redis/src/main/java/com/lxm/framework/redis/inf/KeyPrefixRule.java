package com.lxm.framework.redis.inf;

/**
 * @Author: Lys
 * @Date 2023/2/7
 * @Describe
 **/
public interface KeyPrefixRule {

    String getPrefix();
}
