## lxm-framework-auth

鉴权框架，实现身份、权限、角色认证

### lxm-token 身份认证
即实现登录的会话管理

#### 设计思路
**username** + **password** 登录方式，后端会对身份进行校验，校验成功之后，返回token，在后续的会话交互中，后端会通过前端提交的token进行校验，会话成功。

简单来说，就是后端为用户颁发会话凭证的过程。

#### 名词解释：
- loginId： 账号。 lxm-framework-auth框架并没有实现验证用户身份这一过程，因此name + password提交之后，需要上层来完成校验，校验成功之后，通过传入loginId到框架，换取token。注意保障此loginId在框架里的唯一性，这是区分不同账户的唯一依据。
- token：令牌，校验的凭证，登录成功之后会返回给前端，后续的校验以此为基准。
- device：登录的设备。通过相同的loginId和不同的device可以实现在不同环境下的登录。可以抽象理解此概念，设备可以是真正的设备，例如浏览器，手机app。也可以是虚拟的自定义设备，ip等等。也可以不传此值，默认值为"default-device",注意在不传值的情况下，只能单点登录。
- session：本文档里提到的所有session，指的都是框架里抽象的会话对象，而并非http-servlet，或浏览器里的session。一个session对应一个loginI的，但包含可以包含多个device和其对应的token。即对于同一个账号而言，可以实现多点登录（也可以单点登录，通过配置allowConcurrentLogin实现），但是保存此session对象的那个key，始终是同一个loginId。
- filter：哪些uri需要放行，哪些uri需要登录验证。
- role：角色。在大多数系统里，每个账号都对应了一个或多个角色，而不同角色拥有的权限是不同的。
- permission：权限。同上，权限和角色的区别在于，他们是不同维度的管理，存在包含和被包含的关系。

#### 使用
使用Xmauth静态类里的静态方法即可使用。具体方法内容参考下文。

#### 主要功能和对应接口
##### 功能
- 登录 
- 检查登录状态
- 登出 
- 禁用 
- 通过token禁用 
- 踢人（强制下线）
- 通过token踢人
- 以游客身份登录（临时令牌）
- 获取当前会话的loginId
##### 接口
```java
 	/**
     * 默认方法的登录
     * @param loginId  /
     * @return  /
     */
    default String signIn(String loginId) {
        return signIn(loginId, AuthConstants.DEFAULT_DEVICE);
    }
    
    /**
     * 登录
     * @param loginId  /
     * @param device  /
     * @return   token
     */
    String signIn(String loginId, String device);

    /**
     *  检查登录状态，未登录就抛错
     */
    void checkSignIn();

    /**
     * 返回登录状态，不抛错
     * @return  /
     */
    boolean isSignIn();

    /**
     * 登出
     */
    void signOut();

    /**
     * 禁用
     * @param loginId  /
     * @param forbidTime  /
     */
    void forbid(String loginId,long forbidTime);

    /**
     * 通过token禁用
     * @param token  /
     * @param forbidTime  /
     */
    void forbidByToken(String token,long forbidTime);

    /**
     * 启用forbid的账号
     * @param loginId  /
     */
    void permit(String loginId);

    /**
     * 踢人，强制下线
     * @param loginId  /
     */
    void kick(String loginId);

    /**
     * 通过token踢人
     * @param token  /
     */
    void kickByToken(String token);

    /**
     * 以游客身份登录
     * @return
     */
    String tourist();

    /**
     * 获取当前会话的loginId
     * @return login-id
     */
    String getLoginId();
```
#### 配置
框架可以实现的自定义配置（重写）很多，但是最简单的配置方式只需要2步即可。
第一步：yml里配置框架的基本信息
```yml
lxm-auth:
  token:
    token-name: yolo
    token-prefix: tpp
    allow-concurrent-login: false
```
全部可配置选项如下
```java
	/**
     * token名称 (同时也是cookie名称)
     */
    private String tokenName = "lxmtoken";

    /**
     * token的长久有效期(单位:秒) 默认1天,-1代表一次性
     */
    private long timeout =  24 * 60 * 60;

    /**
     * 一次性token，对于不登录，但又临时授予一个游客账号身份，有效期1小时
     */
    private long onceTimeout = 60 * 60;

    /**
     * 是否允许同一账号并发登录 (为true时允许一起登录, 为false时新登录挤掉旧登录)
     */
    private Boolean allowConcurrentLogin = true;

    /**
     * 在多人登录同一账号时，是否共用一个token (为true时所有登录共用一个token, 为false时每次登录新建一个token)
     */
    private Boolean isShare = true;

    /**
     * 是否尝试从请求体里读取token
     */
    private Boolean isReadParameter = true;

    /**
     * 是否尝试从header里读取token
     */
    private Boolean isReadHead = true;

    /**
     * 是否尝试从cookie里读取token
     */
    private Boolean isReadCookie = true;

    /**
     * token风格(默认可取值：uuid、simple-uuid、random-32、random-64、random-128、tik)
     */
    private String tokenStyle = "random-64";

    /**
     * 是否打开自动续签 (如果此值为true, 框架会在每次直接或间接调用getLoginId()时进行一次过期检查与续签操作)
     */
    private Boolean autoRenew = true;

    /**
     * 写入Cookie时显式指定的作用域, 常用于单点登录二级域名共享Cookie的场景
     */
    private String cookieDomain;

    /**
     * token前缀
     */
    private String tokenPrefix;
```
第二步：实现AuthFilterRegister，重写方法。需要实现的方法如下：
```java
 	/**
     * 需要拦截的uri 例如：/api/v1/**
     *
     * @return
     */
    List<String> includes();

    /**
     * 需要放行，不进行权限校验的uri 例如：/auth/sign-in
     *
     * @return
     */
    List<String> excludes();

    /**
     * 当前访问的uri是需要拦截的uri时，需要执行的内容
     */
    ServletFilterStrategy whenBlocked();

    /**
     * 被 whenBlocked 执行时或许会抛错，然后被此方法捕获
     * 对于捕获的错误，可以根据具体情况返回json-string
     * 最后输出流会写出此内容返回请求方
     *
     * @return
     */
    ServletErrorStrategy whenError();
```
#### 监听器
系统自带了一个默认的监听器，当对应事件发生时触发，如果有需要可以自己实现。
```java
/**
     * on sign-in
     *
     * @param loginId
     * @param token
     */
    void signIn(String loginId, String token);

    /**
     * on create-session
     *
     * @param loginId   /
     * @param token  /
     */
    void createSession(String loginId, String token);

    /**
     * sign-out
     * @param loginId
     * @param device
     */
    void signOut(String loginId, String device);

    /**
     * 以游客身份登录
     * @param loginId /
     * @param token   /
     */
    void tourist(String loginId, String token);

    /**
     * delete session
     * @param loginId
     */
    void deleteSession(String loginId);

    /**
     * forbid a login-id
     * @param loginId
     * @param forbidTime
     */
    void forbid(String loginId, long forbidTime);

    /**
     * kick a login-id
     * @param loginId
     */
    void kick(String loginId);

    /**
     * recover a login-id from forbid to normal
     * @param loginId
     */
    void permit(String loginId);
```


#### 持久层

##### 默认或自定义
众所周知，session,loginId,token是需要记录下来的。框架默认使用lxm-framework-common里的基于内存的TimeCache进行存储。使用者可以通过自己实现接口LxmTokenDao来自定义，接口定义如下：
```java
	// default time-unit = seconds
    TimeUnit SEC_UNIT = TimeUnit.SECONDS;

    // 以下所有的timeout时间单位都是s

    /**
     * 根据key获取value，如果没有，则返回空
     *
     * @param key 键名称
     * @return value
     */
    String get(@NonNull String key);

    /**
     * 写入指定key-value键值对，并设定过期时间 (单位: 秒)
     *
     * @param key     键名称
     * @param value   值
     * @param timeout 过期时间 (单位: 秒)
     */
    void set(@NonNull String key,@NonNull  String value, long timeout);

    /**
     * 修改指定key的剩余存活时间 (单位: 秒)
     *
     * @param key     指定key
     * @param timeout 过期时间
     */
    void update(@NonNull String key,@NonNull  String value, long timeout);

    /**
     * 修改指定key的剩余存活时间 (单位: 秒)
     *
     * @param key     指定key
     * @param timeout 过期时间
     */
    void updateTimeout(@NonNull String key, long timeout);

    /**
     * 删除一个指定的key
     *
     * @param key 键名称
     */
    void delete(@NonNull String key);

    /**
     * 获取指定key的剩余存活时间 (单位: 秒)
     *
     * @param key 指定key
     * @return 这个key的剩余存活时间
     */
    long getTimeout(@NonNull String key);


    //  for session //

    LxmSession getSession(@NonNull String key);

    /**
     * 保存session
     * @param session   /
     * @param timeout   /
     */
    void setSession(@NonNull LxmSession session, long timeout);

    /**
     * 更新session
     * @param session   /
     * @param timeout   /
     */
    void updateSession(@NonNull LxmSession session,long timeout);

    /**
     * 删除session
     * @param session   /
     */
    void deleteSession(@NonNull LxmSession session);

    /**
     * 保存已封禁的账号，注意这里不要直接用login-id为key，会和token的存储对象的key冲突。
     * 建议增加一个前缀为key，例如forbid_loginId的形式。下面isForbid和permit方法同理。
     * @param loginId
     * @param timeout
     */
    void setForbid(String loginId,long timeout);

    /**
     * 是否已被封禁
     * @param loginId
     * @return
     */
    boolean isForbid(String loginId);

    /**
     * 解除封禁
     * @param loginId
     */
    void permit(String loginId);
```
##### 使用redis
框架基于Jedis实现了一套LxmTokenDao的实现，只需要在yml里配置即可使用，参考配置如下：
```yml
lxm-auth:
  redis:
    host: 192.168.115.123
    port: 6379
    password: 123456
    database: 14
```
建议配置此redis的时候，使用的database和其他业务区分开来，不要混用。

#### 其他可自定义实现的功能
- LxmAuthLogic： 框架主要的身份逻辑模块 
- AuthAction： LxmAuthLogic的辅助模块，生成token，写入cookie到http-servlet-response等
- LxmContext： http上下文管理器
- LxmRequest： http请求管理器
- LxmResponse： http响应管理器
- ServletErrorStrategy： 对于配置的需要认证的uri，如果未通过认证，可以通过实现这个接口来向外层抛出一个json-string类型的返回类
- ServletFilterStrategy： 对于配置需要认证的uri，触发认证的时候，可以通过实现这个接口来做一些额外的事情，它就是一个事件触发的回调，ServletErrorStrategy同理


### lxm-token 角色、权限认证
顾名思义，对角色和权限的认证管理
#### 设计思路
假设有一个简化版本的教务管理系统，教师可以在系统上对学生信息进行增删改查，校长可以在系统上对教师信息进行增删改查。那么我们就可以抽象出对权限的管理模式。
- permission 权限：对学生信息的增删改查集合为student.create , student.update , student.delete , student.query。对教师的增删改查集合为teacher.create , teacher.update , teacher.delete , teacher.query
因此，教师的权限应该包含了对学生操作的集合，但不包含对教师的操作集合。校长包含了对学校、教师两者的操作集合。在系统内需要做的事，对每个教师的账号设置对应的权限集合，也对校长赋予对应的权限集合。
- role 角色：角色是对权力分配的抽象概念，在上述这个例子里，教师的账号显然是存在非常多的，如果逐个授权并且维护的话，是显然累赘的，因此可以使用角色管理，仅需建立两个角色teacher,schoolmaster。这样就能区分两者不同的权限了。

#### 使用方式
使用注解的方式，对method或class进行标记，即可实现(放在class的执行顺序高于method，如果class上的标记没有通过，便不会进行method上的校验)。注意，虽然放在service层也可以实现，但是为了语义清晰，应当放在controller层，配合uri使用，这样在维护系统的时候更加直观。

- @LxmCheckPermission 权限管理，包含需要校验的权限字符串数组
- @LxmCheckRole 角色管理，包含需要校验的角色字符串数组
- @LxmCheckOr 包含以上两个标签，可以实现要么校验权限，要么校验角色，两者其中1个通过即可
- @LxmMode 包含在权限管理和角色管理里，有and和or模式，默认值and。and模式下，需要匹配字符串数组里所有内容才可通过，or模式下，只要通过1个即可

#### 示例
- permisson.or，满足其中任一即可
```java 
@LxmCheckPermission(value = {"teacher.query","teacher.create","teacher.update","teacher.delete"},mode = LxmMode.OR)
    @RequestMapping("/perm-or")
    public JsonResult<?> permissionOr(HttpServletRequest request, HttpServletResponse response) {
        return JsonResult.json(0, "perm-or");
    }
```

- permission.and，需要全部满足
```java
@LxmCheckPermission(value = {"teacher.query","teacher.create","teacher.update"})
    @RequestMapping("/perm-and")
    public JsonResult<?> permissionAnd(HttpServletRequest request, HttpServletResponse response) {
        return JsonResult.json(0, "perm-and");
    }
```
-  role.or，满足其中任一即可
```java
@LxmCheckRole(value = {"stuff","teacher","school-master"},mode = LxmMode.OR)
    @RequestMapping("/role-or")
    public JsonResult<?> roleOr(HttpServletRequest request, HttpServletResponse response) {
        return JsonResult.json(0, "role-or");
    }
```
- role.and，需要全部满足
```java
@LxmCheckRole(value = {"stuff","teacher","school-master"})
    @RequestMapping("/role-and")
    public JsonResult<?> roleAnd(HttpServletRequest request, HttpServletResponse response) {
        return JsonResult.json(0, "roleAnd");
    }
```
- checkOr，permisson满足或role满足(具体满足条件又根据标签内的mode来判断)
```java
@LxmCheckOr(permission = @LxmCheckPermission({"teacher.query","teacher.create","teacher.update"}), role = @LxmCheckRole(value = {"school-master"}))
    @RequestMapping("/check-or")
    public JsonResult<?> orAnd(HttpServletRequest request, HttpServletResponse response) {
        return JsonResult.json(0, "check-or");
    }
```

#### 需要实现的方法
在真正的使用之前，需要实现2个接口，对应了role和permission。因为框架并不能知道每个账号对应的role和permission，需要通过实现接口的方式告诉框架。然后把实现类注册为bean即可。
如果没有实现这一步，框架虽然能够正常运行，但是因为上述原因，框架持有的role和permission的集合均为空，无法达到测试效果。
##### roleFilter
```java
/**
     * get role list
     * @param loginId /
     * @return  /
     */
    List<String> getRoleList(String loginId);

    /**
     * has role or not
     * @param loginId   /
     * @param role  /
     * @return  true or false
     */
    boolean hasRole(String loginId,String role);
```

##### permissionFilter
```java
/**
     * get permission list
     * @param loginId   /
     * @return  /
     */
    List<String> getPermissionList(String loginId);

    /**
     * has permission or not
     * @param loginId   /
     * @param permission    /
     * @return  true or false
     */
    boolean hasPermission(String loginId, String permission);
```
列出的方法是一定要实现的，除此之外还有一些default方法，也可以视情况重写，不列出了。

### exceptions
框架会根据情况抛出一些错误，具体错误如下：
- LxmPermissionException permission相关错误
- LxmRoleException role相关错误
- LxmVagueException role或者permission错误，在使用@LxmCheckOr时抛出
- LxmAuthException 身份认证相关错误

对于web服务而言，框架基于JsonResult进行了统一拦截处理并返回前端，处理的过程在lxm-framework-web模块下。当错误被拦截时的示例如下：
```json
<JsonResult>
	<code>2001</code>
	<message>该账号缺少对应操作权限</message>
	<data/>
</JsonResult>
```
如果需要自定义，请参考com.lxm.framework.web.jsonresult.handler.GlobalExceptionHandler
