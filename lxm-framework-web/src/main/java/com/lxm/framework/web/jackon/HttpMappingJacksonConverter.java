package com.lxm.framework.web.jackon;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;

@Component
public class HttpMappingJacksonConverter extends MappingJackson2HttpMessageConverter {

    public HttpMappingJacksonConverter(ObjectMapper objectMapper) {
        super(objectMapper);
    }
}
