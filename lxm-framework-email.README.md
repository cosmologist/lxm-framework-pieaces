## lxm-framework-email
email发送，读取的工具类

内置了几种常用的邮箱类型配置信息：QQ，企业QQ，网易，gmail。分为sender和recipient两个方向。

### EmailCategory
```java
public interface EmailCategory {
    Properties props();
}
```
无论是sender还是recipient都需要进行设置的一个interface，具体的设置参考代码。每个不同邮箱的设置不一定相同，需要参考邮箱自己的帮助文档。包括密码和获取。

### sender
email的发送
首先通过config配置发送的邮箱类型信息，发送的邮箱地址，和密码。再通过链式调用发送。
```JAVA
final EasyEmailSender.EmailSender sender = EasyEmailSender.config(EmailCategorySender.QQ, 		"327266750@qq.com", "poqhkfutorijbhba");
        sender.subject("subject")
                .from("from-who")
                .cc("petrichor1989@yeah.net")
                .text("text")
                .send();
```
### recipient
email的读取
和sender一样，也要配置读取邮箱的类型，地址和密码。
```java
final EasyEmailRecipient recipient = EasyEmailRecipient.config(EmailCategoryRecipient.QQ, "327266750@qq.com", "poqhkfutorijbhba");
        var re = recipient.connect().readThenClose();
        final String content = re.getContent();
        final String fromEmail = re.getFromEmail();
        final String fromName = re.getFromName();
        final String subject = re.getSubject();
        final LocalDateTime receivedAt = re.getReceivedAt();
        log.debug("fromEmail : {} , fromName : {}", fromEmail, fromName);
        log.debug("subject : {},receivedAt : {}", subject, receivedAt);
        log.debug("content : {}",content);
```
readThenClose方法使用之后，不可再次读取，连接已关闭，只能拿到读取内容进行处理。

#### read的几种方法

##### read()
返回邮箱里的第一条

##### read(String fromAddress)
挑选输入的来信地址，返回匹配条件的第一条

##### read(String fromAddress, String subjectCharacterContains)
挑选输入的来信地址，和抬头里的文字进行匹配，返回匹配条件的第一条

还有readThenClose方法，和read一样，只是读取之后就会close掉连接，无法再次读取。