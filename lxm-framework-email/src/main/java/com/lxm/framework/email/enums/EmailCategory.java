package com.lxm.framework.email.enums;

import java.util.Properties;

/**
 * @Author: Lys
 * @Date 2022/9/15
 * @Describe
 **/
public interface EmailCategory {


    Properties props();
}
