package com.lxm.framework.auth.aop;

/**
 * @Author: Lys
 * @Date 2023/6/13
 * @Describe
 **/
public enum LxmMode {

    OR,
    AND
}
