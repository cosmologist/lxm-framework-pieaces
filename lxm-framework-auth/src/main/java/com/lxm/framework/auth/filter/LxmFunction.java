package com.lxm.framework.auth.filter;

/**
 * @Author: Lys
 * @Date 2023/5/30
 * @Describe
 **/
@FunctionalInterface
public interface LxmFunction {

    void run();
}
