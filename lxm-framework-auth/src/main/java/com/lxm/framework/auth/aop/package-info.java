/**
 * @Author: Lys
 * @Date 2023/6/15
 * @Describe
 *
 *      包含业务权限和角色权限的校验
 *      {@link com.lxm.framework.auth.aop.LxmCheckRole}
 *      {@link com.lxm.framework.auth.aop.LxmCheckPermission}
 *      {@link com.lxm.framework.auth.aop.LxmCheckOr}
 *      三者都可以独立存在，且笑果独立
 *      其中1个报错，都会报错。
 **/
package com.lxm.framework.auth.aop;