package com.lxm.framework.excel.inf;

/**
 * Excel 本身数据文件
 * @author by jueyue on 18-4-8.
 */
public interface IExcelDataModel {

    /**
     * 获取行号
     * @return
     */
    int getRowNum();

    /**
     *  设置行号
     * @param rowNum
     */
    void setRowNum(int rowNum);

}
