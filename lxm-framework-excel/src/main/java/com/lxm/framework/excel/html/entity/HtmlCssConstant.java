package com.lxm.framework.excel.html.entity;

/**
 * @author twenty2
 */
public interface HtmlCssConstant {

    /**
     * constants
     */
    String PATTERN_LENGTH = "\\d*\\.?\\d+\\s*(?:em|ex|cm|mm|q|in|pt|pc|px)?";
    String STYLE = "style";
    /**
     * direction
     */
    String TOP = "top";
    String RIGHT = "right";
    String BOTTOM = "bottom";
    String LEFT = "left";
    String WIDTH = "width";
    String HEIGHT = "height";
    String COLOR = "color";
    String BORDER = "border";
    String CENTER = "center";
    String JUSTIFY = "justify";
    String MIDDLE = "middle";
    String FONT = "font";
    String FONT_STYLE = "font-style";
    String FONT_WEIGHT = "font-weight";
    String FONT_SIZE = "font-size";
    String FONT_FAMILY = "font-family";
    String TEXT_DECORATION = "text-decoration";
    String UNDERLINE = "underline";
    String ITALIC = "italic";
    String BOLD = "bold";
    String NORMAL = "normal";
    String TEXT_ALIGN = "text-align";
    String VETICAL_ALIGN = "vertical-align";
    String BACKGROUND = "background";
    String BACKGROUND_COLOR = "background-color";
    String NONE = "none";
    String HIDDEN = "hidden";
    String SOLID = "solid";
    String DOUBLE = "double";
    String DOTTED = "dotted";
    String DASHED = "dashed";

}
