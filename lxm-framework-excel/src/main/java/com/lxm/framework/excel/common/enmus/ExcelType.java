package com.lxm.framework.excel.common.enmus;

/**
 * Excel Type
 * @author twenty2
 */
public enum ExcelType {
        /**
         * 03
         */
        HSSF ,
        /**
         * 07-later
         */
        XSSF
}
