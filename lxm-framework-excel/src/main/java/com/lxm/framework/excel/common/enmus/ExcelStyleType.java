package com.lxm.framework.excel.common.enmus;


/**
 * 插件提供的几个默认样式
 *
 * @author twenty2
 */
public enum ExcelStyleType {

    /**
     * 默认
     */
    NONE,
    /**
     * 数据居中
     */
    DATA,
    /**
     * 数据左对齐
     */
    DATA_LEFT,
    /**
     * 数据右对齐
     */
    DATA_RIGHT,
    /**
     * 金钱数额
     */
    MONEY
}
