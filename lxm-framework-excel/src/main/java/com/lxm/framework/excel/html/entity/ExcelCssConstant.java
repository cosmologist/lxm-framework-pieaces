package com.lxm.framework.excel.html.entity;

/**
 * Excel 自定义处理的自定义Html标签
 *
 * @author twenty2
 */
public interface ExcelCssConstant {

    String SHEET_NAME = "sheetName";
    String FREEZE_ROW = "freezeRow";
    String FREEZE_COL = "freezeCol";

}
