# lxm-framework-pieaces

### 介绍
包含了权限、email、excel、mongo、mybatisplus、redis、web的框架集合。根据需要单独导入。

### 软件架构
基于spring-boot2.7.7及以上版本。基于JAVA17+

### 使用说明
参考各个README文档


