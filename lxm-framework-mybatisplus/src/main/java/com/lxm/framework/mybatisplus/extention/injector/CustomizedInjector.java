package com.lxm.framework.mybatisplus.extention.injector;

import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.injector.methods.*;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

/**
 * @Author: Lys
 * @Date 2022/2/28
 * @Describe
 **/
@Slf4j
public class CustomizedInjector extends DefaultSqlInjector {

    @Override
    public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
        var list = Stream.of(
                new Insert(),
                new com.lxm.framework.mybatisplus.extention.injector.methods.Delete(),
                new com.lxm.framework.mybatisplus.extention.injector.methods.DeleteByMap(),
                new Update(),
                new UpdateById(),
                new SelectById(),
                new SelectBatchByIds(),
                new SelectByMap(),
                new SelectOne(),
                new SelectCount(),
                new SelectMaps(),
                new SelectMapsPage(),
                new SelectObjs(),
                new SelectList(),
                new SelectPage()
        ).collect(toList());
        if (tableInfo.havePK()) {
            list.addAll(List.of(new com.lxm.framework.mybatisplus.extention.injector.methods.DeleteById(),
                    new com.lxm.framework.mybatisplus.extention.injector.methods.DeleteBatchByIds(),
                    new UpdateById(),
                    new SelectById(),
                    new SelectBatchByIds()
            ));
        } else {
            this.logger.warn(String.format("%s ,Not found @TableId annotation, Cannot use Mybatis-Plus 'xxById' Method.", tableInfo.getEntityType()));
        }
        return list;
    }
}
