package com.lxm.framework.mybatisplus.common;

/**
 * @Author: Lys
 * @Date 2022/2/25
 * @Describe
 **/
public class SqlConstant {

    public static final String TID = "tenantId";

    public static final String UPDATED_AT = "updatedAt";

    public static final String CREATED_AT = "createdAt";

    public static final String UPDATED_BY_ID = "updatedById";

    public static final String CREATED_BY_ID = "createdById";

    public static final String DELETED_TIME = "deletedTime";

    public static final String DELETED_TIME_DEFAULT = "1900-01-01 00:00:00";

    public static final String DELETED = "deleted";

    public static final String FLAG_DELETE = "delete";

    public static final String FLAG_DELETE_SHORT = "del";

    public static final String FLAG_REMOVE = "remove";

    public static final String FLAG_ESCAPE = "$";
}
