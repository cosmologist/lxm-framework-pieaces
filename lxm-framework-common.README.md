## lxm-framework-common
所有framework-piece的共用类，包含了一些基础的枚举、定义、exceptions等

### 工具类

| CryptoUtils       | 加密工具，尚未包含解密。主要做单向加密 |
| ----------------- | -------------------------------------- |
| DateTimeUtils     | 时间类工具                             |
| DoubleUtils       | double的数学运算                       |
| ListUtils         | 分隔数组 根据每段数量分段              |
| MarkdownUtils     | md工具                                 |
| ObjectMapUtils    | Object 和 map之间的转换工具            |
| StringFormatUtils | String的格式化、校验等工具             |
| UrlUtils          | url工具，提取url，uri等                |

### 定义
| StandardPrinciple | 后期对Auth模块的抽象化      |
| ----------------- | --------------------------- |
| Result            | web交互的返回对象           |
| AppException      | 全局的exception             |
| StandardPage      | 分页的interface             |
| BaseError         | 所有错误警告枚举的interface |
| CommonError       | 一些基础的错误告警枚举      |

### cache功能
重点模块，包含了2部分，一部分caffeine的内存缓存模块，直接使用。第二部分是自己写的customized-cache，下文介绍

#### immutable-key
不可变包装类。用作cache里的key的封装层。
在abstract-cache里，可以以任意类型为key存储对象，但是这样存在一个问题，即有2个内容完全相同的对象A,B，他们因为equels方法不同，在底层map判断时会识别为2个不同的key，这对于业务使用来说是有问题的。因此为了实现任意类型的key的存储，需要一个包装类，重写其equals方法，使得只要内容相同的对象，都被认定为相同，以达到key可以被正常使用的目的。
```java
public interface Immutable<T> extends Serializable {

    T get();

}
```
重写equals方法
```java
public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (this.getClass() == obj.getClass()) {
            final ImmutableKey<?> that = (ImmutableKey<?>) obj;
            return this.key.equals(that.key);
        }
        return false;
    }
```

#### cache-target
value的存储类型。为了实现独立区分每个存储到cache里的对象都有单独的过期时间、访问次数的维护，再嵌套了一层K,V形式的cache-target。这里面的key没有用imutableKey，因为在上层的cache已经做到了key的区分。
属性如下：
```java
	protected final K key;
    protected final V obj;
    /**
     * 上次访问时间
     */
    protected volatile long lastAccess;
    /**
     * 时间格式
     */
    protected final TimeUnit unit;
    /**
     * 访问次数
     */
    protected volatile AtomicLong accessCount = new AtomicLong();
    /**
     * 到期时间 local-date-time
     */
    private LocalDateTime expireTime;
    /**
     * 到期时间 millis
     */
    private long expireTimeMillis;
    /**
     * 是否永久存储
     */
    private boolean permanent;
    /**
     * 过期时间的timestamp 
     */
    private final long expire;
````


#### cache-listener
一个监听的functional-interface，作用是在移除出内存时起到回调作用。对于同一个cache的实例，只存在一个listener，换句话说，这个listener对全局生效。
```java
@FunctionalInterface
public interface CacheListener<K, V> {

    void onRemove(K k,V v);
}
```

#### cache
接口，定义了行为
```java
public interface Cache<K, V> extends Serializable, Iterable<CacheTarget<K, V>> {

    /**
     * 不传入时间，默认永久存储
     *
     * @param key
     * @param value
     */
    default void put(K key, V value) {
        put(key, value, 0, null);
    }

    /**
     * 完整的存储方式
     *
     * @param key
     * @param value
     * @param timeout
     * @param timeUnit
     */
    void put(K key, V value, long timeout, TimeUnit timeUnit);

    default V get(K key) {
        return get(key, false);
    }

    V get(K key, boolean refresh);

    /**
     * get不到的时候，做otherwise的事情
     *
     * @param key       key
     * @param otherwise 不然就做。。
     * @return
     */
    default V get(K key, Function<K, V> otherwise) {
        V value = get(key);
        if (null != value) {
            return value;
        }
        return otherwise.apply(key);
    }

    V getThenRemove(K key);

    boolean exist(K key);

    boolean isFull();

    void remove(K key);

    void clear();

    int size();

    void onRemove(K key, V value);

    void expireListener(CacheListener<K, V> cacheListener);
}
```

#### abstract-cache
内部属性有3个
```java
 	protected CacheListener<K, V> listener;

    protected Map<Immutable<K>, CacheTarget<K, V>> cacheMap;
    /**
     * 访问次数，get，set方法每调用一次，+1
     */
    protected LongAdder visitCount = new LongAdder();
```
cacheMap是主要的存储对象，在具体的实现类permanent-cache和time-cache里，以concurrentHashMap实现。


####  permenent-cache
一个不主动删除就永久存储的cache，可以理解为一个map。没有什么好说的

####  time-cache
定时功能的cache，通过定时器实现清除过期k，v对的功能。主要通过ScheduledExecutorService实现。
默认定时轮询时间是30s一次，因此如果存储的键值对生命周期在<=30s时，可能不会被严格执行。例如定时器走到25秒时，存了一个6秒的键值对，也需要经过35秒才会被清除。因此对时间敏感的存储对象，不适合使用此cache。
