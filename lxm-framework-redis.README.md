## lxm-framwork-redis
redis的工具类，包含了存储功能的RedisStorage，锁功能的RedisLocker，消息队列功能的RedisBus

### RedisDbConfig
遵守RedisProperties的定义，按照格式配置yml即可，系统会自动提取配置文件然后初始化redis。在存储功能上，对于对象的序列化/反序列化采用了Jackson。暂不支持多个database的配置方式。

### RedisClient
是RedisStorage，RedisLocker，RedisBus的窗口，要使用具体的实现类，强烈建议直接通过RedisClient获取。对于具体的实现，因为需要RedisTemplate或RedisContainer的构造，因此不建议单独构造，而应当直接使用RedisClient.
示例如下：

```java
		final RedisBus bus = RedisClient.messageBus();
        final RedisLocker locker = RedisClient.locker();
        final RedisStorage storage = RedisClient.storage();
```
### RedisStorage
存储器，底层是RedisTemplate，实现了常用的T类型和List<T>类型的存储，设置过期时间，设置过期监听等。
RedisStorage的使用分为两部分，前部分是一些基础配置的行为，例如prefix，key，expire,setLisetner的动作(还有delete和deleteBatch这样，只需要key就能完成的动作)。在执行完基础行为之后，使用use()方法，进入后部分。后部分就是常规的Redis的对value的一些操作，无需多言。
以下是一个简单的示例：

用两部分完成一个string value的存储：

```java
	RedisClient.storage()
                .prefix("prefix")
                .key("key")
                .expire(10, TimeUnit.MINUTES)
                .use()
                .store(RandomStringUtils.randomAlphabetic(32));
```
第一部分里可以完成删除操作：
```java
	RedisClient.storage()
                .prefix("prefix")
                .key("key")
                .delete();
```

#### RedisOnRemoveListener
通过RedisStorage第一部分的listener方法嵌入删除监听。和TimeCache不同的是，这个过期的监听是独立开的，每个不同的K,V具有不同的监听。但是仅仅在主动删除时生效，对于redis过期之后自动删除的K,V并不会生效，因为redis实行lazy delete策略，而且在已经移除之后才会通知，此时通知的只有key，而没有value，故而达不到监听K,V的目的。

### RedisLocker
锁，底层是StringRedisTemplate，原理是存储一个key到redis里，并设置过期时间。在这个key尚未过期或未主动删除之前，下一个同样的key会被打回。每个不同的key持有的锁，包括它的过期时间，获取间隔都是独立的，可以单独设置。
#### 默认值
||||
| ---- | ---- | ---- |
|     expire |   锁的持有时间   |    5s  |
|    duration  |   锁的尝试获取间隔时间   |  0.5s    |

注意，对于每个锁，都有一个默认的最大的持有时间限制，这个值是60s，如果尝试设置expire的值>60s，不会生效。
在执行获取锁之前，会进行一次参数设置。也可以使用无需等待的方式调用，当使用这种方式时，如果没有获取到锁就会立刻抛出错误RedisLockerOccupiedException。

```java
/**
     * duration 不能小于 expire
     * 若小于，则令其相等
     */
    private void checkExpireAndDuration() {
        final Long expire = core.getExpire();
        final Long duration = core.getDuration();
        final TimeUnit expireUnit = core.getExpireUnit();
        final TimeUnit durationUnit = core.getDurationUnit();
        if (expireUnit.toMillis(expire) < durationUnit.toMillis(duration)) {
            core.setDuration(expire).setDurationUnit(expireUnit);
        }
    }
```
#### 加锁或获取锁，tryLock
```java
public boolean tryLock() {
        // 计算锁的过期时间
        long expireTime = System.currentTimeMillis() + core.getExpire();
        // 设置锁的key和value
        Boolean result = session.opsForValue().setIfAbsent(core.getFinKey(), String.valueOf(expireTime), core.getExpire(), TimeUnit.MILLISECONDS);
        if (result != null && result) {
            // 设置成功，表示获取锁成功
            return true;
        }
        // 设置失败，获取锁的value，判断是否超时
        String currentValue = session.opsForValue().get(core.getFinKey());
        if (currentValue != null && Long.parseLong(currentValue) < System.currentTimeMillis()) {
            // 超时，重新设值
            result = session.opsForValue().setIfPresent(core.getFinKey(), String.valueOf(expireTime), core.getExpire(), TimeUnit.MILLISECONDS);
            return null != result && result;
        }
        // 其他情况，表示获取锁失败
        return false;
    }
```
#### 主动释放锁
```java
public void releaseLock() {
        // 检查参数
        checkExpireAndDuration();
        // 获取锁的value，判断是否是当前线程设置的
        String currentValue = session.opsForValue().get(core.getFinKey());
        if (currentValue != null) {
            // 使用del命令删除key，释放锁
            session.delete(core.getFinKey());
        }
    }
```
#### 获取资源
```java
public void accessResource(boolean hold) throws RedisLockerOccupiedException {
        // 尝试获取锁
        boolean fetch = tryLock();
        long holding = 0L;
        boolean throwIt;
        for (throwIt = false; !fetch; fetch = this.tryLock()) {
            // 如果不等，就直接抛错
            if (!hold) {
                throwIt = true;
                break;
            }
            // 没获取到，就等
            try {
                TimeUnit.MILLISECONDS.sleep(core.getDuration());
                holding += core.getDuration();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (holding > core.getExpire()) {
                throwIt = true;
                break;
            }
        }
        if (throwIt) {
            throw new RedisLockerOccupiedException();
        }
    }
```
#### 获取资源，执行一个runnable，然后释放锁
```java
public void accessResourceThenRelease(Runnable task) throws RedisLockerOccupiedException {
        accessResource();
        task.run();
        releaseLock();
    }
```
### RedisBus
用redis实现的消息队列，subscriber/publisher功能，顾名思义，分为了2个部分。
底层使用RedisTemplate和RedisMessageListenerContainer实现，前者作为publisher发送事件的底层，后者作为subscriber注册到容器并接受到事件的底层。

#### Subscriber
订阅者，以String类型的topic为标记，当publisher发布事件之后，subscriber会接收到它发布的Object。接受一个BiConsumer<String, Object>()以处理回调事件。
订阅者的订阅可以通过方法once()指定只需要订阅一次，触发即销毁。也可以通过persist()方法指定永久生效。
对于永久生效的订阅者，每执行一次完整的（即从RedisClient.messageBus().subscriber()开始执行）调用链路，就会生成一次全新的回调consumer，即使topic相同，也会对不同的consumer都触发一次回调，为了避免执行逻辑的冲突，请注意。
```java
RedisClient.messageBus()
                .subscriber()
                .persist()//永久生效
                //once() 生效一次即销毁
                .topic("topic-name")
                .consumer(new BiConsumer<String, Object>() {
                    @Override
                    public void accept(String s, Object o) {
                        // todo
                    }
                }).subscribe();
```

#### publisher
发布者，同样以String类型的topic为标记，使用payload()方法放入需要订阅者消费的对象
```java
RedisClient.messageBus()
         .publisher()
         .topic("topic-name")
         .payload(RandomStringUtils.randomAlphabetic(32))
         .publish();
```
需要注意两点：

1. 发布者发布的消息如果一直没能被消费，也并不会被gc掉，会一直留存且占用内存空间。因此使用的时候应避免这样的情况出现。
2. 对于相同的topic，只要是在发布者发布前创建的消费者，无论多少个，都能通知到消费者手里，而不会只通知到部分。反之，不会被通知到。

